/*
* @Author               : Ajitesh
* @Description          : APex class for creating new Person Account record and calculating total visit
* @Date Of Creation     : Feb 26-2019
*/
public class LeadHandler{
    /*
	* Description: create new Person Account record
	*/
	public static void createPersonAccount(List<Lead> newLeads){
		List<Account> newAccounts = new List<Account>();
		Account acc;
        Set<String> emails = new Set<String>();
        Map<String,Account> emailAccountMap = new Map<String,Account>();
        for(Lead ld:newLeads){
            if(ld.Email!=null)
                emails.add(ld.Email.trim());
        }
        if(!emails.isEmpty()){
            for(Account accRec:[Select Id, PersonEmail, Total_Visits__c FROM Account WHERE PersonEmail IN:emails]){
                emailAccountMap.put(accRec.PersonEmail,accRec);
            }
        }
            
		for(Lead ld:newLeads){
			if(ld.Generated_From_WebToLead__c){
				acc = new Account();
				acc.FirstName = ld.FirstName;
				acc.LastName = ld.LastName;
				acc.PersonEmail = ld.Email;
				acc.Phone = ld.Phone;
				acc.Preferred_Phone_Number__c = ld.Preferred_Phone_Number__c;
				acc.Office__c = ld.Office__c;
				acc.Zip__c = ld.Zip__c;
				acc.Patient_Advocate__c = ld.Patient_Advocate__c;
				acc.How_d_you_hear_hear_about_us__c = ld.How_d_you_hear_hear_about_us__c;
                acc.Account_Date__c = ld.Account_Date__c;
                acc.Total_Visits__c = 1;
                acc.Attorneys_list__c = ld.Attorneys_list__c;
                if(emailAccountMap.containsKey(acc.PersonEmail)){
                    acc = emailAccountMap.get(acc.PersonEmail);
                    acc.Total_Visits__c = (acc.Total_Visits__c!=null ? acc.Total_Visits__c+1:1);
                }
				newAccounts.add(acc);
			}
		}
		
        System.debug('newAccounts @>'+newAccounts);
		if(!newAccounts.isEmpty()){
			Database.upsert(newAccounts,false);
		}
	}
    
    /*
	* Description: logic for calculating lead total visit
	*/
    public static void updateTotalVisit(List<Lead> newLeads, Map<Id,Lead> oldLeads){
        //create collection of lead emails
        Set<String> leadEmails = new Set<String>();
        Set<Id> leadIds = new Set<Id>();
        for(Lead ld:newLeads){
            if(Trigger.isInsert) {            
            	if(ld.Email!=null) 
                	leadEmails.add(ld.Email);
            } else if(Trigger.isUpdate){
                if(ld.Email!=oldLeads.get(ld.Id).Email && ld.Email!=null) 
                	leadEmails.add(ld.Email);
            	leadIds.add(ld.Id);
            }
        }
        
        if(!leadEmails.isEmpty()){            
            Map<String,Integer> leadCount = new Map<String,Integer>();
            
            //count existing lead with same email
            for(Lead oldLead:[Select Id,Email,Total_Visit__c FROM Lead 
                              WHERE Email IN:leadEmails AND 
                              Id Not IN:leadIds AND
                              Total_Visit__c<>null 
                              Order By Total_Visit__c DESC]){
                if(!leadCount.containsKey(oldLead.Email)){
                    leadCount.put(oldLead.Email,Integer.valueOf(oldLead.Total_Visit__c));
                }
            }
            
            //update lead total visit
            for(Lead ld:newLeads){            
                ld.Total_Visit__c = 1;
                if(leadCount.containsKey(ld.Email)){
                    ld.Total_Visit__c = leadCount.get(ld.Email)+1;
                    leadCount.put(ld.Email,Integer.valueOf(ld.Total_Visit__c));
                } 
            }
        }
    }
}