/*
* @Author               : Ajitesh
* @Description          : Test class for LeadHandler
* @Date Of Creation     : Feb 26-2019
*/
@isTest class LeadHandlerTest {
	static testMethod void unitTest(){
		Lead ld = new Lead(
			FirstName='test',
			LastName='test',
			Email='test@test.test',
			Generated_From_WebToLead__c=true);
		insert ld;
        
        ld = new Lead(
			FirstName='test1',
			LastName='test1',
			Email='test@test.test',
        	Generated_From_WebToLead__c=true);
		insert ld;
	}
}