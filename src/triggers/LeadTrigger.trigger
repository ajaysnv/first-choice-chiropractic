/*
* @Author               : Ajitesh
* @Description          : Lead trigger 
* @Date Of Creation     : Feb 26-2019
*/
trigger LeadTrigger on Lead(before insert,before update,after insert){
	if(Trigger.isAfter && Trigger.isInsert)
    	LeadHandler.createPersonAccount(trigger.new);
    else if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
        LeadHandler.updateTotalVisit(trigger.new,trigger.oldMap);
}